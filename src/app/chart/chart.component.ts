import {Component, Inject, NgZone, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';

// amCharts imports
import am5geodata_worldTimeZoneAreasLow from "@amcharts/amcharts5-geodata/worldTimeZoneAreasLow";
import am5geodata_worldTimeZonesLow from "@amcharts/amcharts5-geodata/worldTimeZonesLow";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import * as am5 from "@amcharts/amcharts5";
import * as am5map from "@amcharts/amcharts5/map";


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent {
  private root!: am5.Root;
  HHstart = 17 // when happy hour starts
  HHend = 20 // when happy hour ends

  constructor(@Inject(PLATFORM_ID) private platformId: Object, private zone: NgZone) {
  }

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
        /* Chart code */
// Create root element
// https://www.amcharts.com/docs/v5/getting-started/#Root_element
        let root = am5.Root.new("chartdiv");

// Set themes
// https://www.amcharts.com/docs/v5/concepts/themes/
        root.setThemes([
          am5themes_Animated.new(root)
        ]);

// Create the map chart
// https://www.amcharts.com/docs/v5/charts/map-chart/
        let chart = root.container.children.push(
          am5map.MapChart.new(root, {
            panX: "translateX",
            panY: "translateY",
            projection: am5map.geoMercator()
          })
        );

        let colorSet = am5.ColorSet.new(root, {});

// Create main polygon series for time zone areas
// https://www.amcharts.com/docs/v5/charts/map-chart/map-polygon-series/
        let areaSeries = chart.series.push(
          am5map.MapPolygonSeries.new(root, {
            geoJSON: am5geodata_worldTimeZoneAreasLow
          })
        );


        let areaPolygonTemplate = areaSeries.mapPolygons.template;
        areaPolygonTemplate.setAll({fillOpacity: 0.6});
        areaPolygonTemplate.adapters.add("fill", function (fill, target) {
          return am5.Color.saturate(
            colorSet.getIndex(areaSeries.mapPolygons.indexOf(target)),
            0.3
          );
        });
        areaPolygonTemplate.states.create("hover", {fillOpacity: 0.8});

// Create main polygon series for time zones
// https://www.amcharts.com/docs/v5/charts/map-chart/map-polygon-series/
        let zoneSeries = chart.series.push(
          am5map.MapPolygonSeries.new(root, {
            geoJSON: am5geodata_worldTimeZonesLow
          })
        );

        zoneSeries.mapPolygons.template.setAll({
          fill: am5.color(0x000000),
          fillOpacity: 0.2
        });

        let zonePolygonTemplate = zoneSeries.mapPolygons.template;
        // console.log(zoneSeries.mapPolygons.template);
        // zoneSeries.mapPolygons.template.set();
        zonePolygonTemplate.setAll({interactive: true, tooltipText: "{id}"});
        zonePolygonTemplate.states.create("hover", {fillOpacity: 0.3});

// labels
        let labelSeries = chart.series.push(am5map.MapPointSeries.new(root, {}));
        labelSeries.bullets.push(() => {
          return am5.Bullet.new(root, {
            sprite: am5.Label.new(root, {
              text: "{id}",
              populateText: true,
              centerX: am5.p50,
              centerY: am5.p50,
              fontSize: "0.7em"
            })
          });
        });

        let timeZone = am5geodata_worldTimeZonesLow.features.map(feat => feat.id);
        // console.log(am5geodata_worldTimeZonesLow)
        // let aperoZone = timeZone.filter(tz => tz)
        // console.log(hour)

        // console.log(this.utcToLocalDate("UTC-07:45"))

// create labels for each zone
        zoneSeries.events.on("datavalidated", () => {
          am5.array.each(zoneSeries.dataItems, (dataItem) => {
            let countryDate: string
            if (dataItem.get("id")) {
              if (this.utcToLocalDate(dataItem.get("id") as string)) {
                dataItem.get("mapPolygon").setAll({
                  fill: am5.color(0x000000),
                  fillOpacity: 0.8
                });
              }
            }
          });
        });

// Add zoom control
// https://www.amcharts.com/docs/v5/charts/map-chart/map-pan-zoom/#Zoom_control
        chart.set("zoomControl", am5map.ZoomControl.new(root, {}));

// Add labels and controls
        let cont = chart.children.push(
          am5.Container.new(root, {
            layout: root.horizontalLayout,
            x: 20,
            y: 40
          })
        );

        cont.children.push(
          am5.Label.new(root, {
            centerY: am5.p50,
            text: "Map"
          })
        );

        let switchButton = cont.children.push(
          am5.Button.new(root, {
            themeTags: ["switch"],
            centerY: am5.p50,
            icon: am5.Circle.new(root, {
              themeTags: ["icon"]
            })
          })
        );

        switchButton.on("active", function () {
          if (!switchButton.get("active")) {
            chart.set("projection", am5map.geoMercator());
            chart.set("panX", "translateX");
            chart.set("panY", "translateY");
          } else {
            chart.set("projection", am5map.geoOrthographic());
            chart.set("panX", "rotateX");
            chart.set("panY", "rotateY");
          }
        });

        cont.children.push(
          am5.Label.new(root, {
            centerY: am5.p50,
            text: "Globe"
          })
        );
// Make stuff animate on load
        chart.appear(1000, 100);
      }
    );
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.root) {
        this.root.dispose();
      }
    });
  }


  private utcToLocalDate(utcString: string) { //"UTC-07:00"
    const sign = utcString.includes("+") ? "+" : "-";
    const hoursOffset = parseInt(utcString.substring(4, 6));
    const minutesOffset = parseInt(utcString.substring(7, 9));

    const date = new Date();
    let UTCzeroHours = date.getUTCHours(); // UTC-0 hours
    let localhour = 0;
    if (sign === "+") {
      localhour = UTCzeroHours + hoursOffset;
    } else {
      localhour = UTCzeroHours - hoursOffset;
    }
    return ((localhour >= this.HHstart) && (localhour <= this.HHend));
  }
}
